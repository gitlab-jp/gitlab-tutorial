## テキスト

- [Rails tutorial (日本語版)](https://railstutorial.jp/chapters/account_activation?version=5.0#cha-account_activation)

### 1. トピックブランチを作成

以下のコマンドを実行して、ワーキングディレクトリを最新のmasterと同期させて下さい。

```
$ cd ~/workspace/sample_app
$ git checkout master
$ git pull
```

以下のコマンドを実行して、開発サーバーを起動し、ブラウザで簡単に動作確認を行い、前回までの作業内容を確認して下さい。

```
$ rails server -b $IP -p $PORT
```

1. ユーザー一覧の表示
2. ユーザーの削除


確認が終わったら`control + C`キーでサーバーを終了し、以下のコマンドを実行して、今回のトピックブランチを作成して下さい。

```
$ git checkout -b account-activation
```

### 2. AccountActivationsリソース

アカウントの有効化という作業を「リソース」としてモデル化します。

- コードの修正箇所: 8fc478899d4b46ec3a407f5b08cfb08961350ff3

#### 機能説明

1. AccountActivationsコントローラを追加
2. `edit`アクションへの名前付きルートを定義
3. Userモデルにアカウント有効化用の属性を追加
4. User生成時にアカウント有効化トークンとダイジェストを生成し、ダイジェストを保存

#### 注意点

1. AccountActivationsコントローラの作成(追加)は以下のコマンドで行って下さい。
```
$ rails generate controller AccountActivations
```
2. マイグレーションファイルの作成は以下のコマンドで行ってください。
  (`db/migrate/[timestamp]_add_activation_to_users.rb`ファイルを手で追加しない)
```
$ rails generate migration add_activation_to_users activation_digest:string activated:boolean activated_at:datetime
```
3. 手で`db/migrate/schema.rb`を編集しないでください。
  (`$ rails db:migrate`を実行すると自動的に更新される)

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

### 3. アカウント有効化のメール送信

アカウント有効化メールを送信する。

- コードの修正箇所: 0dedd25474c3558873d4399c8b9a7ba90d83a233

#### 機能説明

1. UserのMailerを追加
2. 送信メールのプレビュー
3. ユーザーの作成時にアカウント有効化メールを送信し、トップページにリダイレクト

#### 注意点

1. UserMailerの作成(追加)は以下のコマンドで行ってください
```
$ rails generate mailer UserMailer account_activation password_reset
```
2. "config/environments/development.rb"の`host`は各自の環境に合わせて変更してください
    * Cloud9の確認用URLから先頭の"https://"を削除した部分

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

開発サーバーを起動して以下の動作確認を行って下さい

1. アカウント有効化メールのプレビューを表示
    * "https://`<your host>`/rails/mailers/user_mailer/account_activation"をアドレスバーに直接入力
    * `<your  host>`は各自の環境に合わせて変更してください
2. アカウントの登録
    * トップページにリダイレクトされること
    * アカウントの有効化メールが送信されること
        * 実際には送信されない
        * ターミナルのログにメールの内容が表示される
    * 有効化されていないアカウントが追加されていること
        * `rails console`で確認
        * ユーザーの取得例: `user = User.where(email: "foo@example.com").first`
        * 有効化の確認例: `user.activated?`

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'アカウント有効化のメール送信'
```

### 4. アカウントを有効化する

アカウント有効化メールのリンクにアクセスすることで、アカウントを有効化する

- コードの修正箇所: c60f108bc976acbaff64dfceac7a2d115c0e1b6b

#### 機能説明

1. Userモデルのauthenticated?メソッドでアカウント有効化トークンを認証
    * ログインのremember機能で追加したメソッドを抽象化する
2. AccountActivationsコントローラのeditアクションでユーザーを有効化
    * 正しいメールアドレスとトークンの組み合わせの場合はユーザーを有効化し、ログイン状態でユーザーページへリダイレクト
    * ただし、既に有効化済みの場合はログイン状態にはせずトップページへリダイレクト
    * 不正なメールアドレスとトークンの組み合わせの場合はトップページへリダイレクトし、エラーメッセージを表示

#### 注意点

1. 特になし

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

開発サーバーを起動して以下の動作確認を行って下さい

1. アカウント有効化メールのリンクにアクセスすることで以下の処理が実行されること
    1. アカウントが有効化されること(`rails console`で確認)
    2. ログイン状態でユーザーページへリダイレクトされること
2. 上記で有効化後に同じリンクにアクセスするとログイン状態にならないこと
3. 不正なメールアドレスとトークンの組み合わせの場合はアカウントを有効化できないこと
    1. リンク内のメールアドレス、またはトークンを手で変更して、ブラウザのアドレスバーに直接入力する

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'アカウントを有効化する'
```

### 5. 本番環境でのメール送信

herokuにクレジットカード情報を登録する必要があるので、講義では扱いません。

代わりに[letter_opener_web](https://github.com/fgrehm/letter_opener_web)を使用します。

- 変更内容: 35aae407a76e6ddc01228f4de036cb2ccb22df68

#### 注意点

1. `Gemfile.lock`を手で編集しないでください
  (`Gemfile`修正後に`bundle install`コマンドを実行すると自動的に更新されます)

#### 動作確認

開発サーバーを起動して以下の動作確認を行って下さい

1. アカウントの登録後に`/letter_opener`のURLに直接アクセスし、アカウント有効化メールが届いていること

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'メール送信にletter_opener_webを使用する'
```

### 6. 最後に

今回の修正内容をGitLabにpushし、Merge Requestを作成し、masterブランチにマージしてください。

```
$ git push
```
