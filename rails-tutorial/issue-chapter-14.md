## テキスト

- [Rails tutorial (日本語版)](https://railstutorial.jp/chapters/following_users?version=5.0#cha-following_users)

### 1. トピックブランチを作成

以下のコマンドを実行して、ワーキングディレクトリを最新のmasterと同期させて下さい。

```
$ cd ~/workspace/sample_app
$ git checkout master
$ git pull
```

以下のコマンドを実行して、開発サーバーを起動し、ブラウザで簡単に動作確認を行い、前回までの作業内容を確認して下さい。

```
$ rails server -b $IP -p $PORT
```

1. マイクロポストを投稿できること

確認が終わったら`control + C`キーでサーバーを終了し、以下のコマンドを実行して、今回のトピックブランチを作成して下さい。

```
$ git checkout -b following_users
```

### 2. Relationshopモデル

ユーザーをフォローする、ユーザーからフォローされるを表現するRelationshopモデルを追加します。

#### 機能説明

1. Relationshopモデルを新規作成
2. Relationshopのバリデーションを追加
3. UserとRelationshopを関連付け

#### コードの修正点

- コミット: 760b0e112c4a06760026bb4be681b320b0a8d01e
- 注意点
    1. 最初に以下のコマンドでマイグレーションファイルを作成する
    ```
    $ rails generate model Relationship follower_id:integer followed_id:integer
    ```
    2. 上記で作成された`db/migrate/[timestamp]_create_relationships.rb`を編集する
    3. 上記の編集後に以下のコマンドでDBをマイグレーションする
    ```
    $ rails db:migrate
    ```
    4. 以下のファイルを手で編集しない
        - `db/schema.rb`

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'Relationshopモデル'
```

### 3. [Follow] のWebインターフェイス

Followしているユーザー(following)とFollowされているユーザー(follower)を表示する。

#### 機能説明

1. トップページとユーザーのshowページにfollowingとfollowerの件数を表示する
2. followingとfollowerの一覧ページを追加
3. ユーザーのshowページにfollowとunfollowのボタンを追加
    1. followとunfollowの操作をAJAX化する

#### コードの修正点

- コミット: 837ba33d36b56d3abdb8759d2bd2447276f08831
- 注意点
    1. 最初に`rails generate`コマンドでファイルの生成を行う
    ```
    $ rails generate integration_test following
    $ rails generate controller Relationships
    ```
    2. ファイルの修正後に以下のコマンドでDBのデータを作り直す
    ```
    $ rails db:migrate:reset
    $ rails db:seed
    ```

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

開発サーバーを起動して以下の動作確認を行って下さい。
確認の際は以下のアカウントを使用してください。

- Account: `example@railstutorial.org`
- Password: `foobar`

1. トップページとユーザーのshowページにfollowingとfollowerの件数が表示されること
2. followingとfollowerのリンクをクリックしてそれぞれの一覧ページが表示されること
3. ユーザーのshowページでfollowとunfollowができること

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m '[Follow] のWebインターフェイス'
```

### 4. ステータスフィード

トップページにユーザーにフォローされているユーザーのマイクロポストと、
現在のユーザー自身のマイクロポストを合わせて表示する。

以下、ステータスフィードと呼ぶ。

#### 機能説明

1. トップページにステータスフィードを表示する
2. ステータスフィードの取得にサブクエリを使用する

#### コードの修正点

- コミット: 4c8043f34ef6af7ff542b31df8103c1c036954dd

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

開発サーバーを起動して以下の動作確認を行って下さい

1. トップページにステータスフィードが表示されること

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'ステータスフィード'
```

### 5. 最後に

今回の修正内容をGitLabにpushし、Merge Requestを作成し、masterブランチにマージしてください。

興味のある方は、Rials Tutorialの「サンプルアプリケーションの機能を拡張する」に挑戦してみて下さい。
