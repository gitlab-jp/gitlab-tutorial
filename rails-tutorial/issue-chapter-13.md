## テキスト

- [Rails tutorial (日本語版)](https://railstutorial.jp/chapters/user_microposts?version=5.0#cha-user_microposts)

### 0. 豆知識

- [GitLabの障害について](http://www.publickey1.jp/blog/17/gitlabcom56.html)
- [RESTfulとは](http://www.slideshare.net/tkawa1/learning-rest-from-rails-style)

### 1. トピックブランチを作成

以下のコマンドを実行して、ワーキングディレクトリを最新のmasterと同期させて下さい。

```
$ cd ~/workspace/sample_app
$ git checkout master
$ git pull
```

以下のコマンドを実行して、開発サーバーを起動し、ブラウザで簡単に動作確認を行い、前回までの作業内容を確認して下さい。

```
$ rails server -b $IP -p $PORT
```

1. パスワードリセットリクエスト
    * `/letter_opener`にアクセスしてメールを確認
    * パスワードを変更


確認が終わったら`control + C`キーでサーバーを終了し、以下のコマンドを実行して、今回のトピックブランチを作成して下さい。

```
$ git checkout -b user-microposts
```

### 2. Micropostsモデル

Micropostリソースを表現するMicropostモデルを作成します。

#### 機能説明

1. Micropostモデルを新規作成
2. Micropostのバリデーションを追加
3. UserとMicropostの関連付け

#### コードの修正点

- コミット: a4e977d8c7c778c183d00e1627722e10688a7821
- 注意点
    1. 最初に`rails generate`コマンドでファイルの作成を行う
    ```
    $ rails generate model Micropost content:text user:references
    ```
    2. 今回追加されるファイルは全てコマンドで自動的に作成されるので手で追加しない
    3. `db/schema.rb`を手で変更しない
    3. ファイルの修正後に以下のコマンドを実行しDBをマイグレションする
    ```
    $ rails db:migrate
    ```

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'Micropostモデル'
```

### 3. マイクロポストを表示する

ユーザーのshowページにマイクロソフトを表示する

#### 機能説明

1. ユーザーのshowページにマイクロソフトを表示する
    * 1ページに表示するのは30件まで
2. マイクロポストを`$ rails db:seed`で自動生成する

#### コードの修正点

- コミット: d8ab63ef6a0402d71910435785d9cb30b9143d8a
- 注意点
    1. 最初に`rails generate`コマンドでファイルの生成を行う
    ```
    $ rails generate controller Microposts
    $ rails generate integration_test users_profile
    ```
    2. ファイルの修正後に以下のコマンドでDBのデータを作り直す
    ```
    $ rails db:migrate:reset
    $ rails db:seed
    ```

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

開発サーバーを起動して以下の動作確認を行って下さい

1. ユーザー(`example@railstutorial.org`)のshowページにマイクロポストが表示されること

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'マイクロポストを表示する'
```

### 4. マイクロポストを操作する

マイクロポストを作成、削除する機能を追加します。

#### 機能説明

1. ログイン済の場合にホーム画面にマイクロポストを投稿するフォームを表示
2. ログイン済の場合にホーム画面にマイクロポストのフィードを表示
3. フィードの削除リンクをクリックするとマイクロポストが削除される

#### コードの修正点

- コミット: 66354f5596644465ca841875ca6d048483a6413a
- 注意点
    1. 最初に`rails generate`コマンドでファイルの作成を行う
    ```
    $ rails generate integration_test microposts_interface
    ```

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

開発サーバーを起動して以下の動作確認を行って下さい

1. ログイン済みの場合にホーム画面で以下の操作ができること
    1. マイクロポストを投稿する
    2. マイクロポストを削除する

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'マイクロポストを操作する'
```

### 5. マイクロポストの画像投稿

マイクロポストに画像を投稿する機能を追加します。

#### 機能説明

1. マイクロポストの投稿フォームに画像項目を追加
2. 画像の拡張子とサイズでバリデーション
3. 画像の解像度が大きい場合は400x400ピクセルにリサイズ

#### コードの修正点

- コミット: 20b6f5bbe8bc4dc31b3093c60ec54f8ead53de85
- 注意点
    1. 最初に`Gemfile`を編集する
    2. 上記の編集後に以下のコマンドを実行しgemをインストールする
    ```
    $ bundle install
    ```
    3. 上記の実行後に以下のコマンドを実行しPictureUploaderを作成する
    ```
    $ rails generate uploader Picture
    ```
    4. 上記の実行後に以下のコマンドを実行しMicropostモデルにpicture属性を追加する
    ```
    $ rails generate migration add_picture_to_microposts picture:string
    $ rails db:migrate
    ```
    5. `db/migration/[timestamp]_add_picture_to_microposts.rb`は上記コマンドの実行時刻によってファイル名が異なるので注意する
    6. 以下のファイルを手で編集しない
        - `Gemfile.lock`
        - `db/schema.rb`

#### 動作確認

1. ターミナルで`$ rails test`を実行してテストがGREENとなること

開発サーバーを起動して以下の動作確認を行って下さい

1. ログイン済みの場合にホーム画面で以下の操作ができること
    1. マイクロポストに画像を投稿できること

動作確認が終わったら変更内容をコミットして下さい。

```
$ git add .
$ git commit -m 'マイクロポストの画像投稿'
```
### 6. 最後に

今回の修正内容をGitLabにpushし、Merge Requestを作成し、masterブランチにマージしてください。

なお、本番環境のHerokuではアップロードしたファイルは一時的にしか保存されません。
そのため、Rails TutorialではAWS S3に保存する方法が説明されていますが、講義では扱いません。

興味のある方は、Rials Tutorialを参考に挑戦してみて下さい。
